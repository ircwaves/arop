This is a reference repository for the **A**utomated **R**egistration and
**O**rthorectification **P**ackage (AROP).  AROP is developed by [Feng
Gao](https://www.ars.usda.gov/northeast-area/beltsville-md-barc/beltsville-agricultural-research-center/hydrology-and-remote-sensing-laboratory/people/feng-gao/)
at the USDA-ARS.
